package helpers;

import java.util.ArrayList;
import java.util.List;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.time.LocalDateTime; 
import javax.swing.JOptionPane;

import database.PlayerDao;
import database.gamesDao;
import memoreyes.controllers.BotController;

import memoreyes.controllers.CardBoardController;

import memoreyes.controllers.GameController;
import memoreyes.controllers.PlayerController;
/**
 * 
 * @author racks
 * Game Helper Class 
 * Checks for Correct picked cards from user and Robot and the neccessery actions to 
 * represent that data in some of the Views
 * Helps with the game logic and info
 */
public class GameHelper {
    public GameController theGame;
    public String player;
    private static GameHelper instance;
    public int counter = 0;
    public boolean gotACard = false;
    public PlayerController playerCont;
    public BotController botCont;
    public int card1=0, card2=0,botcard1=0,botcard2=0;
    public int cordI, cordY;
    public CardBoardController cbdView;
    public List<Integer> Vpaths = new ArrayList<Integer>();
    public int position1,position2;
    

    private GameHelper() {
    }
    /**
     * method that calls the resetBoard from CardBoardView
     * @throws IOException 
     */
    public void cardBoardDoReset() throws IOException {
        cbdView.resetBoard(cordI, cordY);

    }
    /**
     * stores the player's found cards
     */
    public void addPlayerFoundCards() {
    	
        playerCont.setPlayerFoundCards(1);
        theGame.gameModel.setPlayerScore(playerCont.cardsFoundByPlayer());
 
    }
/**
 * checking if the picked cards from user are the same or not
 * @throws IOException 
 */

	public void check() throws IOException {
 
        if (card1 == card2) {
            //System.out.print("\n card 1 id "+ position1+"\n card 2 id "+ position2);
            Vpaths.add(card1);
            addPlayerFoundCards();
            theGame.changeScore(playerCont.cardsFoundByPlayer(),botCont.bot.getPoints());
            
            if (((cordI * cordY) / 2) == Vpaths.size()) {
               theGame.changeScore(playerCont.cardsFoundByPlayer(),botCont.bot.getPoints());
               decideWinner();
   
            }
            
        } else {

            
                          
        }
        cardBoardDoReset();
        botCont.pickCards();
        
    }
    /**
 * checking if the picked cards from bot are the same or not
     * @throws IOException 
 */
    public void checkBot() throws IOException{
        if ( botcard1 ==  botcard2) {

            Vpaths.add(botcard1);
            botCont.setBotPoints();
            theGame.changeScore(playerCont.cardsFoundByPlayer(),botCont.bot.getPoints());
            
 
 
        } else {
 
        }
        cardBoardDoReset();
 
    }
    /**
     * Singleton Pattern 
     * @return 
     */
    public static GameHelper getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new GameHelper();
        return instance;
    }
    /**
     *method that sets the game's info 
     */
    
    public void setGameInfo() {
    	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
    	   LocalDateTime now = LocalDateTime.now();  
       theGame.gameModel.setBotScore(botCont.bot.getPoints());
       theGame.gameModel.setPlayerScore(playerCont.humanPlayer.getFoundCards());
       theGame.gameModel.setDate(dtf.format(now));
       
       new PlayerDao(playerCont.humanPlayer);
       new gamesDao(theGame.gameModel);
    }
    /**
     * method that decides the winner of the current game
     */
    public void decideWinner() {
    	
    	if(theGame.gameModel.getBotScore()>theGame.gameModel.getPlayerScore()) {
    		theGame.gameModel.setWinner("Bots Have Won");
    		JOptionPane.showMessageDialog(null,"My Goodness, Robot Domination Begun! -(|o...o|)-");
    	}
    	else if(theGame.gameModel.getBotScore()<theGame.gameModel.getPlayerScore()) {
    		theGame.gameModel.setWinner("Humans Have Won");
    		JOptionPane.showMessageDialog(null,"My Goodness, You saved the world from Robot Domination!");
    	}
    	else {
    		theGame.gameModel.setWinner("It's a draw");
    		JOptionPane.showMessageDialog(null,"For now on, Humans and bots live in Harmony!");
    	}
    	setGameInfo();
    }
    /**
     * Method that Helps with the Restart of the game
     */
    public void clearAll() {
    	
        player=null;
        counter = 0;
        playerCont=null;
        botCont=null;
        Vpaths = new ArrayList<Integer>();
        theGame.changeScore(playerCont.cardsFoundByPlayer(),botCont.bot.getPoints());
    }
}
