package memoreyes.views;
import javax.swing.*;

import java.awt.event.*;

/**
 * Class that creates the game menu on the top left corner
 * @author racks
 */
public class MenuView extends JMenuBar implements ActionListener {
	    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JFrame f;       
	public JMenu game,options,history,help;    
	public JMenuItem newGame,restart,exit,historyItem1,historyItem2,about;      
	public MenuView(){  
		
   
	restart=new JMenuItem("Restart");    
	exit=new JMenuItem("Exit");    
	historyItem1=new JMenuItem("Show History");
	historyItem2=new JMenuItem("Hide History");
	newGame=new JMenuItem("New Game");
	about=new JMenuItem("about");
	
	restart.addActionListener(this);    
	exit.addActionListener(this);    
	historyItem1.addActionListener(this);
	
	newGame.addActionListener(this);   
	about.addActionListener(this);
	 
	game=new JMenu("Game");    
	options=new JMenu("Options");    
	history=new JMenu("History"); 
	help=new JMenu("Help");
	game.add(newGame);
	game.add(restart);
	game.add(exit);
	history.add(historyItem1);
	history.add(historyItem2);
	options.add(history);
	help.add(about);
	this.add(game);
	this.add(options);
	this.add(help);
	      
	}   
	
	public void actionPerformed(ActionEvent e) {    
	
//	if(e.getSource()==restart){System.out.println("Restart");}     
//	if(e.getSource()==exit){System.out.println("Exit");}      
	if(e.getSource()==about){JOptionPane.showMessageDialog(null, "Velissaris-Racks\nBabis-Kawaii_kitsune\nArtwork: by Us\nThanks for playing!");}    
 
	}  
}
