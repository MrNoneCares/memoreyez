/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.views;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.*;

/**
 *Class that is responsible to get the user's data
 * @author racks
 */
public class PlayerView extends JPanel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // Font
    public Font font = new Font("Trebuchet MS", Font.BOLD, 20);
    // Difficulty-ComboBox
    public JLabel labelChoose = new JLabel("Choose Difficulty");
    public String Array[] = { "Easy", "Normal", "Hard" };
    public JComboBox difficultyDropdown = new JComboBox(Array);
    // NameField-Label
    public JLabel labelName = new JLabel("Players Name :");
    public JTextField nameTextField = new JTextField("Player's Name", 20);
    // Button
    public JButton initButton = new JButton("Submit");
    // Panels
    public JPanel dificultyPanel = new JPanel(new FlowLayout());
    public JPanel Button = new JPanel(new FlowLayout());
    public JPanel playerPanel = new JPanel(new FlowLayout());

    public PlayerView(boolean state) {
        initComp(state);
    }

    public void initComp(boolean state) {
        if (state == false) {
            welcomeScreen();
        } else {
            System.out.println("Timmy go for help");
        }
    }

    void welcomeScreen() {
        // Font's and Tooltips and Images
        //this.setBackground(Color.magenta);
        // imageLabel=new JLabel(image);
        nameTextField.setToolTipText("Please enter a name");
        // Sizing
        initButton.setPreferredSize(new Dimension(100, 30));
        // Difficulty
        labelChoose.setPreferredSize(new Dimension(100, 30));
        difficultyDropdown.setPreferredSize(new Dimension(100, 30));
        // Name
        nameTextField.setPreferredSize(new Dimension(50, 35));
        this.setPreferredSize(new Dimension(150, 100));
        //this.setBackground(Color.black);
        //
        // this.header.add(imageLabel);
        this.playerPanel.add(labelName);
        this.playerPanel.add(nameTextField);
        this.dificultyPanel.add(labelChoose);
        this.dificultyPanel.add(difficultyDropdown);
        this.Button.add(initButton);
        this.add(playerPanel);
        this.add(dificultyPanel);
        this.add(Button);
    }
}
