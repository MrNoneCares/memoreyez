/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.views;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.json.simple.parser.ParseException;


import database.HistoryTable;
import helpers.*;
import memoreyes.controllers.GameController;

/**
 *
 * @author racks
 */
public class MainWindow extends JFrame {
    // handlePanel=GeneralPanel

    /**
     *
     */
	int dim=40;
	JPanel temp=new JPanel();
	HistoryTable historyTable;
	GameHelper helper;
    private static final long serialVersionUID = 1L;
    JPanel handlePanel = new JPanel(new GridLayout(2, 0, 10, 10));
    Font font = new Font("Trebuchet MS", Font.BOLD, 24);
    JLabel labelNewGame = new JLabel("New Game");
    JPanel header = new JPanel(new FlowLayout());
    JPanel forPlayer = new JPanel(new FlowLayout());
    PlayerView usedByPlayer;
    BufferedImage human = ImageIO.read(getClass().getResourceAsStream("/images/human.png"));
    BufferedImage bot = ImageIO.read(getClass().getResourceAsStream("/images/bot.png"));
    public JLabel scoreboard= new JLabel();
    public JLabel humanIcon= new JLabel();
    public JLabel botIcon= new JLabel();
    public JPanel scoreBoardPanel= new JPanel();
    public MenuView menuview= new MenuView();
    public MainWindow() throws IOException {
        super("MemorEyez");
        initComp();
    }
/** method that initializes the view of the app
 * 
 * @throws IOException 
 */
    public void initComp() throws IOException {
    	
    	BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/images/empty.png"));
    	this.setIconImage(image);
        this.setJMenuBar(menuview);
        this.menuview.exit.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			pullThePlug();
    		}
    	});
        this.menuview.newGame.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			setVisible(false); //you can't see me!
    			dispose();
    			try {
					GameController game = new GameController();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}//Destroy the JFrame object
 catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    	});
        this.menuview.restart.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			helper.getInstance().botCont.bot.setPoints(0);
    			helper.getInstance().playerCont.setPlayerFoundCards(0);
    			helper.getInstance().theGame.changeScore(0,0);
    			helper.getInstance().Vpaths.clear();
    			helper.getInstance().cbdView.restartBoard(helper.getInstance().cordI,helper.getInstance().cordY);
    		}
    	});
        this.menuview.historyItem1.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			try {
					showHistory();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    	});
        this.menuview.historyItem2.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			try {
					hideHistory();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    	});
        
        this.setPreferredSize(new Dimension(500, 500));
        // newGame Label
        this.labelNewGame.setFont(font);
        this.labelNewGame.setPreferredSize(new Dimension(150, 30));
        this.header.add(labelNewGame);
        this.header.setPreferredSize(new Dimension(50, 50));
        // General Panel inserts
        this.handlePanel.add(this.header);
        
        this.add(handlePanel);
        pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        Action logout = new AbstractAction()
        {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			/**
			 * 
			 */

			public void actionPerformed(ActionEvent e)
            {
                dispose();
                try {
					GameController game=new GameController();
				} catch (SQLException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        };
         
        InactivityListener listener = new InactivityListener(this, logout,10);
        listener.start();
    }
    /**
     * adds the necessery labels to be shown
     * @param p 
     */

    public void addPlayerLabels(PlayerView p) {
        usedByPlayer = p;
        this.handlePanel.add(usedByPlayer);
        this.handlePanel.revalidate();
        this.handlePanel.repaint();
        this.revalidate();
        this.repaint();
    }
/**
 * method that creates the game interface
 * @param c
 * @throws IOException 
 */
    public void gameMode(CardBoardView c) throws IOException {
        this.handlePanel.remove(header);
        this.handlePanel.remove(usedByPlayer);
        this.handlePanel.setLayout(new FlowLayout());
        this.handlePanel.removeAll();
        this.handlePanel.add(c);
        botIcon.setIcon(new ImageIcon(bot.getScaledInstance(dim-5, dim-5,Image.SCALE_AREA_AVERAGING)));
        humanIcon.setIcon(new ImageIcon(human.getScaledInstance(dim, dim,Image.SCALE_AREA_AVERAGING)));
        this.scoreBoardPanel.add(humanIcon);
        this.scoreBoardPanel.add(scoreboard);
        this.scoreBoardPanel.add(botIcon);
        
        this.handlePanel.add(scoreBoardPanel);
        this.revalidate();
        this.repaint();
        
    }
    public void showHistory() throws IOException, ParseException {
    	temp=this.handlePanel;
    	historyTable =new HistoryTable();
    	this.remove(handlePanel);
    	this.add(historyTable);
//        this.handlePanel.revalidate();
//        this.handlePanel.repaint();
        this.revalidate();
        this.repaint();
    }
    public void hideHistory() throws IOException, ParseException {
    	this.remove(historyTable);
    	this.add(handlePanel);
//    	this.handlePanel=temp;
//        this.handlePanel.revalidate();
//        this.handlePanel.repaint();
        this.revalidate();
        this.repaint();
    }
    public void pullThePlug() {
        setVisible(false);
        dispose();
        // if you have other similar frames around, you should dispose them, too.

        // finally, call this to really exit. 
        // i/o libraries such as WiiRemoteJ need this. 
        // also, this is what swing does for JFrame.EXIT_ON_CLOSE
        System.exit(0); 
    }
}
