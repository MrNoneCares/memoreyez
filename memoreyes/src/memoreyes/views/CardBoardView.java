/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.views;

import memoreyes.controllers.*;
import memoreyes.models.CardBoard;

import java.awt.GridLayout;
import java.io.IOException;
import java.util.List;
import javax.swing.JPanel;

/**
 *Class used to Represent the CardBoard Visually to the User and other Classes as well
 * @author racks
 */
public class CardBoardView extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	List<CardController> cl;
	int count = 0;
	public CardBoard cardBoard=new CardBoard();
	
	public CardBoardView(List<CardController> cardsList, int i, int j) throws IOException {
		initComp(cardsList, i, j);
	}
/**
 * method that makes a list of CardControllers so it can help out the CardBoard(view,controller) 
 * @param cardsList
 * @param i
 * @param j
 * @throws IOException 
 */
	public void initComp(List<CardController> cardsList, int i, int j) throws IOException {
		cardBoard.setdX(i);
		cardBoard.setdY(j);
		cardBoard.setCardsList(cardsList);
		this.setLayout(new GridLayout(i, j, 1, 1));
		
		for (int k = 0; k < i; k++) {
			for (int l = 0; l < j; l++) {
				this.add(cardBoard.getCardsList().get(count).cardView);
				count++;
			}
		}

	}
}
