/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.views;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import helpers.GameHelper;
import memoreyes.models.CardModel;

/**
 *Class that represents the View of the Card 
 * so it can be Showed to the CardBoard View and User
 * @author babis_san
 */
public class CardView extends JLabel {
    /**
    	 *
    	 */
    private static final long serialVersionUID = 1L;
    GameHelper helper;
    int x=140;
    public boolean selected;
    public ImageIcon flipped, faceUp,empty;
    public int path_Ver;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
    	this.cardModel.setPosition(position);
        this.position = position;
    }
    public int position;
    BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/images/flipped.png"));
    BufferedImage empty2 = ImageIO.read(getClass().getResourceAsStream("/images/empty.png"));
    static CardModel cardModel=new CardModel();
    
    public CardView(ImageIcon path, int path_Ver, int position) throws IOException {
    	setCardModel(path,path_Ver,position);
    	setPosition(position);
        initComp(path, path_Ver);
    }
/** making the card
 * 
 * @param path
 * @param path_Ver
 * @throws IOException 
 */
    public void initComp(ImageIcon path, int path_Ver) throws IOException {
    	this.setPreferredSize(new Dimension(x, x));
    	Image dimg = image.getScaledInstance(x, x,Image.SCALE_AREA_AVERAGING);
        ImageIcon imageIcon = new ImageIcon(dimg);
        Image img= cardModel.getPath().getImage();
        img=img.getScaledInstance(x, x,Image.SCALE_AREA_AVERAGING);
        ImageIcon imageIcon2 = new ImageIcon(img);
        this.empty = imageIcon;
        this.faceUp = imageIcon2;
        this.path_Ver = path_Ver;

        this.flipped = imageIcon;
        this.setIcon(flipped);

        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                try {
					faceUp(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });

    }
/**
 * method that changes the view off a card when picked 
 * @param called_By 
 * @throws IOException 
 */
    public void found() throws IOException{
    	Image dimg = empty2.getScaledInstance(x, x,Image.SCALE_AREA_AVERAGING);
        ImageIcon imageIcon = new ImageIcon(dimg);
        
       
        TimerTask t = new TimerTask() {
            public void run() {
            	 setIcon(imageIcon);
            	 revalidate();
                 repaint();
                 disable();
            }
        };
        Timer timer = new Timer();
        timer.schedule(t, 500);
        
        
    }
    
    public void faceUp(boolean called_By) throws IOException {
        this.selected = true;
        this.setIcon(faceUp);
        this.revalidate();
        this.repaint();
        helper.getInstance().counter++;
        if(called_By==true){
        	
            if (helper.getInstance().card1 == 0) {
                helper.getInstance().position1=getPosition();
                helper.getInstance().card1 = this.path_Ver;
            } else if (helper.getInstance().card2 == 0) {
                helper.getInstance().position2=getPosition();
                helper.getInstance().card2 = this.path_Ver;
                helper.getInstance().check();
                helper.getInstance().card1 = 0;
                helper.getInstance().card2 = 0;
                
            }
        }
        else if(called_By==false){
            if (helper.getInstance().botcard1 == 0) {
            	helper.getInstance().position1=getPosition();
                helper.getInstance().botcard1 = this.path_Ver;
            }
            else if (helper.getInstance().botcard2 == 0) {
            	helper.getInstance().position2=getPosition();
                helper.getInstance().botcard2 = this.path_Ver;
                helper.getInstance().checkBot();
                helper.getInstance().botcard1 = 0;
                helper.getInstance().botcard2 = 0;
                
            }
        }

    }
/**
 * get the image path of card
 * @return 
 */
    public String getPath() {
        return faceUp.toString();

    }
/**
 * flips down the card
 */
    public void faceDown() {
        TimerTask t = new TimerTask() {
            public void run() {
                selected = false;
                setIcon(flipped);
            }
        };
        Timer timer = new Timer();
        timer.schedule(t, 1000);
    }

	public CardModel getCardModel() {
		return cardModel;
	}

	public void setCardModel(ImageIcon path,int path_Ver, int position) {
		cardModel.setPath(path);
    	cardModel.setId(path_Ver);
        cardModel.setPosition(position);
	}
	
	public int getPath_Ver() {
		return path_Ver;
	}
	public void setPath_Ver(int path_Ver) {
		this.path_Ver = path_Ver;
	}

}
