/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.models;

import javax.swing.ImageIcon;


/**
 *
 * @author racks
 * card blueprint
 */
public class CardModel {

    public ImageIcon getPath() {
        return Path;
    }

    public void setPath(ImageIcon Path) {
        this.Path = Path;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    public ImageIcon Path;
    public int Id, position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
