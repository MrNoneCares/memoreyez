/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.models;

import java.util.List;

import memoreyes.controllers.CardController;

/**
 *
 * @author racks
 * card board blueprint
 */
public class CardBoard {
    int dX,dY;
    List<CardController> cardsList;
	public List<CardController> getCardsList() {
		return cardsList;
	}

	public void setCardsList(List<CardController> cardsList) {
		this.cardsList = cardsList;
	}

	public int getdX() {
		return dX;
	}

	public void setdX(int dX) {
		this.dX = dX;
	}

	public int getdY() {
		return dY;
	}

	public void setdY(int dY) {
		this.dY = dY;
	}
    
}
