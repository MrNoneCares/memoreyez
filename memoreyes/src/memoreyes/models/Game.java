/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.models;

/**
 *
 * @author racks
 * The Game Blueprint
 */
public class Game {
    boolean State;
    String Difficulty;
    String Winner;
    String PlayerName;
    int playerScore,botScore;
    String date;
   
    public Game(){
    }
    public String getPlayerName() {
		return PlayerName;
    	
    }
    public void setPlayerName(String name) {
    	PlayerName=name;
    	
    }
    public int getBotScore() {
		return botScore;
	}
    
	public void setBotScore(int botScore) {
		this.botScore = botScore;
	}

	public int getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

    public boolean isState() {
        return State;
    }

    public void setState(boolean State) {
        this.State = State;
    }

    public String getWinner() {
        return Winner;
    }

    public void setWinner(String Winner) {
        this.Winner = Winner;
    }
    
    public String getDifficulty() {
        return Difficulty;
    }

    public void setDifficulty(String Difficulty) {
        this.Difficulty = Difficulty;
        
    }
    
    
}
