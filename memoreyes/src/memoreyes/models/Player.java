/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.models;

/**
 *
 * @author racks
 * Player BluePrint
 */
public class Player {
    int ID, foundCards;
    String UserName;

    Boolean Playing;

    public Player() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getFoundCards() {
        return foundCards;
    }

    public void setFoundCards(int foundCards) {
        this.foundCards = foundCards;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
        
    }

    public Boolean getPlaying() {
        return Playing;
    }

    public void setPlaying(Boolean Playing) {
        this.Playing = Playing;
    }

}
