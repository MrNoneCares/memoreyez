/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.controllers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

import memoreyes.views.*;
import memoreyes.models.*;
import helpers.*;

/**
 *
 * @author babis_san
 * Game Controller most of the game's logic is here
 */
public class GameController {
	int milli;
    public Game gameModel = new Game();
    //public Player player= new Player();
    CardBoard cardBoard;
    
    MainWindow mainWindow; 
    PlayerController PlayerController = new PlayerController();
    BotController bot = new BotController();
    GameHelper helper;

    public GameController() throws SQLException, IOException {
    	this.mainWindow = new MainWindow();
        GameHelper.getInstance().theGame = this;
        boolean currentState = gameStatus(true);
        NotifyUser(currentState);
        initComp();
    }
/**
 * notifying user about the current Player's View
 * @param state 
 */
    public void NotifyUser(boolean state) {
        PlayerController.generateViews(PlayerController.playerView, state);
    }
/**
 * Method that makes the main window for the app
 */
    public void initComp() {
    	//helper.getInstance().clearAll();
        PlayerController.playerView.initButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	gameModel.setPlayerName(PlayerController.playerView.nameTextField.getText());
                String difficulty;
                PlayerController.setPlayerDataName(PlayerController.playerView.nameTextField.getText());
                MainWindow tempMainWindow= getMainWindow();
                gameModel.setState(true);
                
                difficulty = PlayerController.playerView.difficultyDropdown.getSelectedItem().toString();
                gameDifficulty(difficulty);
                try {
                	if(gameModel.getDifficulty()=="Hard") {
                		milli=5000;
                		tempMainWindow.setSize(new Dimension(1000,1000));
                		setMainWindow(tempMainWindow);
                		
                	}
                	if(gameModel.getDifficulty()=="Normal") {
                		milli=3000;
                		tempMainWindow.setSize(new Dimension(800,800));
                		setMainWindow(tempMainWindow);
                		
                	}
                	if(gameModel.getDifficulty()=="Easy") {
                		milli=2000;
                		tempMainWindow.setSize(new Dimension(600,600));
                		setMainWindow(tempMainWindow);
                		
                	}
                    CardBoardController cardboard = new CardBoardController(gameModel.getDifficulty());
                    changeScore(0,0);
                    mainWindow.gameMode(cardboard.cardBoardView);
                    GameHelper.getInstance().cbdView = cardboard;
                    GameHelper.getInstance().playerCont = PlayerController;
                    GameHelper.getInstance().botCont = bot;
                    bot.cardBoard=cardboard;
                    helper.getInstance().cbdView.hint(helper.getInstance().cordI, helper.getInstance().cordY,milli);
                } catch (IOException e1) {

                    e1.printStackTrace();
                }

            }
        });
        mainWindow.addPlayerLabels(PlayerController.playerView);
    }
    public boolean gameStatus(boolean state) {
        this.gameModel.setState(state);
        return false;
    }
/**
 * sets the game difficulty also passes the game difficulty to the Game Model
 * @param difficulty 
 */
    public void gameDifficulty(String difficulty) {
        this.gameModel.setDifficulty(difficulty);
    }

	public MainWindow getMainWindow() {
		return mainWindow;
	}

	public void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
		this.mainWindow.revalidate();
		this.mainWindow.repaint();
	}
  /**
   * Method for the Score Label while playing the game
   */
	public void changeScore(int j,int i) {
		// create a line border with the specified color and width
		Border border = BorderFactory.createLineBorder(Color.darkGray, 3);

		// set the border of this component
		mainWindow.scoreboard.setBorder(border);
		mainWindow.scoreboard.setText("  Humans: "+ j+"  .vs.  Robots: "+ i+"  ");
		mainWindow.scoreboard.revalidate();
		mainWindow.scoreboard.repaint();
	}
}
