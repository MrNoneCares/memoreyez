/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.controllers;

import java.sql.SQLException;
import memoreyes.models.Player;
import memoreyes.views.PlayerView;

/**
 * Most of the Player logic is stored here
 * @author racks
 */
public class PlayerController {
    public Player humanPlayer = new Player();
    public PlayerView playerView;
    int foundCards = 0;

    public PlayerController() throws SQLException {

    }

    void setPlayerDataName(String nameFromText) {
        this.humanPlayer.setUserName(nameFromText);
        
    }

    void setPlayerDataID(int id) {
        this.humanPlayer.setID(id);
    }
/**
 * set player's score also it notifyies the Player Model about the Player's score
 */
    public void setPlayerFoundCards(int i) {
    	if(i==0) {
    		this.foundCards=0;
    		}
    	else {
	        this.foundCards+=i;
	        this.humanPlayer.setFoundCards(foundCards);
	        }
    }
/**
 * Get the players score for the app view
 * @return 
 */
    public int cardsFoundByPlayer() {

        return foundCards;
    }
    /**
     * genereate the view for the user
     * @param pview
     * @param state
     * @return 
     */

    PlayerView generateViews(PlayerView pview, boolean state) {
        if (state == false) {
            return playerView = new PlayerView(state);
        } else {
            return playerView = new PlayerView(state);
        }
    }

}
