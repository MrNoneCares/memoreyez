/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.controllers;

import java.io.IOException;
import javax.swing.ImageIcon;

import memoreyes.views.CardView;

/**
 *
 * @author racks
 * The card Controller all the logic for a card is here
 */
public class CardController {
    public CardView cardView;
    public int id;
    public int pathVer;
    public int tempCounter;

    public int getTempCounter() {
        return tempCounter;
    }

    public void setTempCounter(int tempCounter) {
        this.tempCounter = tempCounter;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    public int position;

    public CardController(ImageIcon path, int id, int path_Ver, int position) throws IOException {
        setPathVer(path_Ver);
        setPosition(position);
        setCardView(path, path_Ver,position);
        setId(id);
        
        
        
    }
    public CardView getCardView() {
                
		return cardView;
	}
/**
 * makes a card
 * @param path
 * @param path_Ver
 * @throws IOException 
 */
	public void setCardView(ImageIcon path, int path_Ver, int position) throws IOException  {
		this.cardView = new CardView(path, path_Ver, position);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPathVer() {
		return pathVer;
	}
/**
 * method that sets the cards path 
 * @param pathVer 
 */
	public void setPathVer(int pathVer) {
		this.pathVer = pathVer;
	}

}
