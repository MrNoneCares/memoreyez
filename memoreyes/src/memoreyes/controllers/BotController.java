/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.controllers;

import helpers.GameHelper;

import java.io.IOException;
import java.util.*;

import memoreyes.models.Bot;


/**
 *
 * @author racks
 * the bot's actions are stored in here
 */
public class BotController {
    public CardBoardController cardBoard;
    public GameHelper helper;
    public Bot bot = new Bot();
    int points = 0;
    List < Integer > availablePositions = new ArrayList < Integer > ();
    int hpick, wpick, boardHeight, boardWidth, generalSize;
/**
 * Method that makes the bot to pick a card
 */
    public void pickCards() {
        //System.out.print("\nBeep Boop Robots gonna win!\n");
        availablePositions.clear();
        boardHeight = cardBoard.height;
        boardWidth = cardBoard.width;
        generalSize = boardHeight * boardWidth;
        //System.out.print("\n General Size: "+ generalSize);


        TimerTask t = new TimerTask() {
            public void run() {
 
            try {
				RobotNik();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
        };
        Timer timer = new Timer();
        timer.schedule(t, 2000);
        


    }
    /**
     * method that sets the Bot Model's(or the bot's in general) Points
     */
    public void setBotPoints() {
        this.points++;
       // System.out.println("Bot Points " + this.points);
        bot.setPoints(this.points);
    }
    /**
     * method that kinda acts like a helper for the picking cards method
     * @throws IOException 
     */
    public void RobotNik() throws IOException{
    
                Random rand = new Random();

                int rand_int, rand_two;
                    setAvailablePositions();
                    
                    if(availablePositions.size() != 0){
                    if(availablePositions.size() == 2){
                        rand_int=availablePositions.get(0);
                        rand_two=availablePositions.get(1);
                        cardBoard.cBoard.get(rand_two).cardView.faceUp(false);
                        cardBoard.cBoard.get(rand_int).cardView.faceUp(false);
                        helper.getInstance().decideWinner();
                    }else{
                    rand_int = rand.nextInt(availablePositions.size());
                    cardBoard.cBoard.get(availablePositions.get(rand_int)).cardView.faceUp(false);
                    
                    setAvailablePositions();
                    rand_two = rand.nextInt(availablePositions.size());
                    while (rand_two == rand_int) {
                        rand_two = rand.nextInt(availablePositions.size());
                    }
   
                    setAvailablePositions();
                    //System.out.print("1st card to be clicked " + rand_int);
                    //System.out.print("2nd card to be clicked " + rand_two);
                    for(int i=0;i<availablePositions.size(); i++){
                    //System.out.println("Positions "+availablePositions.get(i));
                    	}
                    cardBoard.cBoard.get(availablePositions.get(rand_two)).cardView.faceUp(false);}
                    }
            }
    /**
     * another kinda helper method that helps with the Robot to see the available Positions in the current deck
     */
    public void setAvailablePositions(){
        availablePositions.clear();
            int counter = 0;
            for (int k = 0; k < boardHeight; k++) {
            for (int l = 0; l < boardWidth; l++) {
                if (cardBoard.cBoard.get(counter).cardView.selected == false) {

                    availablePositions.add(counter);
                    //System.out.print("\nEmpty Seat here: "+ counter);


                } else {
                    
                    //System.out.print("\nNo Empty Seat here"+ counter);
                }
                counter++;
            }
        }}
}
 