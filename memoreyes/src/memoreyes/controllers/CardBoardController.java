/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoreyes.controllers;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import memoreyes.views.*;

import helpers.*;

/**
 *
 * @author racks
 * Controller of the CardBoard
 */
public class CardBoardController {
	public int positionCounter=0;;
    public List<CardController> cBoard = new ArrayList<CardController>();
    public CardBoardView cardBoardView;
    public GameHelper helper;
    public int height,width;
    BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/images/empty.png"));
    public CardBoardController(String s) throws IOException {
        switch (s) {
            case "Easy":
                initBoard(3, 4);
                this.height=3;
                this.width=4;
                GameHelper.getInstance().cordI = 3;
                GameHelper.getInstance().cordY = 4;
                break;
            case "Normal":
                initBoard(4, 5);
                this.height=4;
                this.width=5;
                
                GameHelper.getInstance().cordI = 4;
                GameHelper.getInstance().cordY = 5;
                break;
            case "Hard":
                initBoard(6, 6);
                this.height=6;
                this.width=6;
                GameHelper.getInstance().cordI = 6;
                GameHelper.getInstance().cordY = 6;
                break;
        }
    }
/**
 * method that initializes the card board view
 * @param i
 * @param j
 * @throws IOException 
 */
    public void initBoard(int i, int j) throws IOException {
        int count = 1, countHelp = 0, position=0;
        int pathVer;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
            	
                //System.out.println("i STARTED: " + countHelp);
                //System.out.println(position);
                BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/images/" + count + ".png"));
                pathVer = count;
                CardController c = new CardController(new ImageIcon(image), count, pathVer, position);
                this.cBoard.add(c);
                position++;
                
                if (countHelp == 1) {
                    count++;
                    countHelp = 0;
                } else {
                    countHelp++;
                }
            }
        }      
        Random rand = new Random(); 
        
        
        int rand_int1 = rand.nextInt(1000); 
        Collections.shuffle(this.cBoard,new Random(rand_int1));
        
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
            	this.cBoard.get(positionCounter).cardView.setPosition(positionCounter);
            		positionCounter++;
            	}   
            }   
        
        this.cardBoardView = new CardBoardView(this.cBoard, i, j);

    }
/**
 * method that resets the Card Board View 
 * @param i
 * @param j 
 * @throws IOException 
 */
    //@SuppressWarnings("deprecation")
	public void resetBoard(int i, int j) throws IOException {
        int counter = 0;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {

                if (this.cBoard.get(counter).cardView.selected == true) {
                    if (GameHelper.getInstance().Vpaths.contains(this.cBoard.get(counter).cardView.path_Ver) == true) {
                        this.cBoard.get(counter).cardView.found();
                        
         
                    } else {

                        this.cBoard.get(counter).cardView.faceDown();
                    }
                }
                counter++;
               
            }
        }
    }
    /**
     * Its for restarting the Card Board View after a game Restart 
     * @param i
     * @param j 
     */
    public void restartBoard(int i, int j) {
    	int counter = 0;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {

                this.cBoard.get(counter).cardView.selected = false;
                this.cBoard.get(counter).cardView.faceDown();   
                this.cBoard.get(counter).cardView.enable();
                counter++;
               
            }
        }
    }
    public void hint(int i, int j,int milli) {
    	int counter = 0;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {

                this.cBoard.get(counter).cardView.selected = true;
                this.cBoard.get(counter).cardView.setIcon(this.cBoard.get(counter).cardView.faceUp);   
                
                counter++;
               
            }
        }
        TimerTask t = new TimerTask() {
            public void run() {
            	int counter=0;
            	for (int k = 0; k < i; k++) {
                    for (int l = 0; l < j; l++) {

                        cBoard.get(counter).cardView.selected = false;
                        cBoard.get(counter).cardView.setIcon(cBoard.get(counter).cardView.flipped);   
                        
                        counter++;
                       
                    }
                }
            }
        };
        Timer timer = new Timer();
        timer.schedule(t, milli);
    }
	
    
}
