package database;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import memoreyes.models.Game;
/*
saves the game's info 
*/
public class gamesDao implements Dao{
	public gamesDao(Game g) {
		save(g);
	}
	@Override
	public void getAll(Object t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(Object t) {
		// TODO Auto-generated method stub
		Game game=(Game)t;
		JSONParser jsonParser = new JSONParser();

        try {
        	//Object obj = jsonParser.parse(new FileReader("C:\\Users\\babis_san\\Desktop\\JsonDATA\\gamesData.json"));
            Object obj = jsonParser.parse(new FileReader("./src/database/gamesData.json"));
            JSONArray jsonArray = (JSONArray)obj;

//            System.out.println(jsonArray);
           // {"playerScore":"6","botScore":"6","timeEnded":"2020-04-23T18:28:43.511Z"}
            JSONObject gamesDtata = new JSONObject();
            gamesDtata.put("playerScore",game.getPlayerScore() );
            gamesDtata.put("botScore",game.getBotScore());
            gamesDtata.put("difficulty",game.getDifficulty() );
            gamesDtata.put("winner",game.getWinner() );
            gamesDtata.put("timeEnded",game.getDate());


            jsonArray.add(gamesDtata);

//            System.out.println(jsonArray);

            FileWriter file = new FileWriter("./src/database/gamesData.json");
            file.write(jsonArray.toJSONString());
            file.flush();
            file.close();

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
		
	}

	@Override
	public void update(Object t, String[] params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Object t) {
		// TODO Auto-generated method stub
		
	}

}
