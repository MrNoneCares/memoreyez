package database;
/**
 * Saves player's data 
 */
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import memoreyes.models.Player;

public class PlayerDao implements Dao {
	public PlayerDao(Player p){
		save(p);
	}
	@Override
	public void getAll(Object t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(Object p) {
		Player player= new Player();
		player=(Player) p;
		JSONParser jsonParser = new JSONParser();

        try {
        	//Object obj = jsonParser.parse(new FileReader("C:\\Users\\babis_san\\Desktop\\JsonDATA\\playerData.json"));
            Object obj = jsonParser.parse(new FileReader("./src/database/playerData.json"));
            JSONArray jsonArray = (JSONArray)obj;

//            System.out.println(jsonArray);

            JSONObject user = new JSONObject();
            user.put("id", ""+(jsonArray.toArray().length+1)+"");
            user.put("name", player.getUserName());


            jsonArray.add(user);

//            System.out.println(jsonArray);

            FileWriter file = new FileWriter("./src/database/playerData.json");
            file.write(jsonArray.toJSONString());
            file.flush();
            file.close();

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
		
	}

	@Override
	public void update(Object t, String[] params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Object t) {
		// TODO Auto-generated method stub
		
	}

	
}
