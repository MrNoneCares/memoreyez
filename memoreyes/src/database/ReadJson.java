package database;

import java.io.*;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;   
/**
*
* @author babis_san
* 
*/
public class ReadJson {
	JSONParser parser = new JSONParser();
	public ReadJson() throws FileNotFoundException, IOException, ParseException {
		
		}
	void readPlayer() throws FileNotFoundException, IOException, ParseException {
		JSONArray obj = (JSONArray) parser.parse(new FileReader("src/database/playerData.json"));
		 
		// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.

		for (Object user : obj)
		  {
		    JSONObject person = (JSONObject) user;

		    String id = (String) person.get("id");
		    String name = (String) person.get("name");
		    System.out.println("ID:"+id+" & name:"+name+"\n");

		  }
		
	}
	void readGame() throws FileNotFoundException, IOException, ParseException {
		JSONArray obj2 = (JSONArray) parser.parse(new FileReader("src/database/gamesData.json"));
		 
		// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.

		for (Object game : obj2)
		  {
		    JSONObject games = (JSONObject) game;
		    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		    LocalDateTime now = LocalDateTime.now();  
		    String id = (String) games.get("score");
		    String name = (String) games.get("time");
		    System.out.println("score:"+id+" & timeEnded:"+name+"\n");

		  }
	}
}

