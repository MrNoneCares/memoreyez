package database;

import java.io.*;
import java.util.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;    
/**
*Class that creates a view with History Data form previous games
* @author babis_san
*/
public class HistoryTable extends JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	JFrame f;
    int count=0;
    
public HistoryTable() throws FileNotFoundException, IOException, ParseException{ 
       initTable();
} 
  void initTable() throws FileNotFoundException, IOException, ParseException{
	  String data1=null,data2=null,data3=null,data4=null,data5=null,data6=null,data7=null;	
	    List<String[]> values = new ArrayList<String[]>();
	    List<String> columns = new ArrayList<String>();
	    List<String[]> a= new ArrayList<String[]>();
	    JSONParser parser = new JSONParser();
		JSONArray obj = (JSONArray) parser.parse(new FileReader("./src/database/playerData.json"));
            List<String> dataFor_ID= new ArrayList<String>();
            List<String> dataFor_Name= new ArrayList<String>();
		// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
		columns.add("ID");
                columns.add("NAME");
                columns.add("PLAYER SCORE");
                columns.add("BOT SCORE");
                columns.add("WINNER");
                columns.add("DIFFICULTY");
                columns.add("TIME ENDED");
	    
		for (Object user : obj)
		  {
		    JSONObject person = (JSONObject) user;
		    String id = (String) person.get("id");
		    String name = (String) person.get("name");
                    dataFor_ID.add(id);
                    dataFor_Name.add(name);
	 
		  } 
		JSONArray games = (JSONArray) parser.parse(new FileReader("./src/database/gamesData.json"));
                int helpG=0;
		for (Object game : games)
		  {
		    JSONObject gameObject = (JSONObject) game;
		    String playerscore = (String) gameObject.get("playerScore").toString();
		    String botscore = (String) gameObject.get("botScore").toString();
		    String difficulty = (String) gameObject.get("difficulty");
		    String winner = (String) gameObject.get("winner");
		    String timeEnded = (String) gameObject.get("timeEnded");
		    data3=playerscore;
		    data5=botscore;
		    data4=timeEnded;
		    data6=winner;
		    data7=difficulty;		
                    a.add(new String[] {dataFor_ID.get(helpG),dataFor_Name.get(helpG),data3,data5,data6,data7,data4});
                     helpG++;
		   
		  } 
                
		values.addAll(a);
                
		//values.add(new String[] {data1, data2,data3,data5,data4});
	           
	    TableModel tableModel = new DefaultTableModel(values.toArray(new Object[][] {}), columns.toArray());
	    JTable jt = new JTable(tableModel);    
	    jt.setBounds(30,40,200,300);          
	    JScrollPane sp=new JScrollPane(jt);    
	    this.add(sp);          
	    this.setSize(600,400);    
	    this.setVisible(true); 
  }
}  