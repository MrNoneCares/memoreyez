package database;

public interface Dao<T> {
    
    void getAll(T t);
/**
 * Save info
 * @param t 
 */  
    void save(T t);
 /** method that updates info
  * 
  * @param t
  * @param params 
  */   
    void update(T t, String[] params);
    
    void delete(T t);
}